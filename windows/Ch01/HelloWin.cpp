#include "pch.h"

int main()
{
  SYSTEM_INFO Sys_Info;
  ::GetNativeSystemInfo(&Sys_Info);

  printf("Number of logical processors: %d\n", Sys_Info.dwNumberOfProcessors);
  printf("Page size: %d bytes\n", Sys_Info.dwPageSize);
  printf("Processor mask: 0x%p\n", (PVOID)Sys_Info.dwActiveProcessorMask);
  printf("Minimum process address: 0x%p\n", (PVOID)Sys_Info.lpMinimumApplicationAddress);
  printf("Maximum process address: 0x%p\n", (PVOID)Sys_Info.lpMaximumApplicationAddress);
}