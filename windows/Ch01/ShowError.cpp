#include "pch.h"

int main(int argc, const char* argv[])
{
        if (argc < 2) {
                printf("Usage: ShowError <number>\n");
                return 0;
        }

        int msg = atoi(argv[1]);

        LPWSTR text;
        DWORD chars = ::FormatMessage(
                FORMAT_MESSAGE_ALLOCATE_BUFFER | 
                FORMAT_MESSAGE_FROM_SYSTEM | 
                FORMAT_MESSAGE_IGNORE_INSERTS,
                nullptr, msg, 0, (LPWSTR)&text, 0, nullptr);
        if (chars > 0) {
                printf("Message %d: %ws\n", msg, text);
                ::LocalFree(text);
        } else {
                printf("No such error exists\n");
        }
        
        return 0;
}

/* Execution example commands
        C:\Dev\Win10SysProg\x64\Debug>ShowError.exe 2
                Message 2: The system cannot find the file specified.
        C:\Dev\Win10SysProg\x64\Debug>ShowError.exe 5
                Message 5: Access is denied.
        C:\Dev\Win10SysProg\x64\Debug>ShowError.exe 129
                Message 129: The %1 application cannot be run in Win32 mode.
        C:\Dev\Win10SysProg\x64\Debug>ShowError.exe 1999
                No such error exists
*/