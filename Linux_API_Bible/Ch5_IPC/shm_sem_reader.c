#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <semaphore.h>

#define SHM_NAME "/my_shared_memory"
#define SEM_NAME "/my_semaphore"

typedef struct {
    char message[100];
} SharedData;

int main() {
    int shm_fd;
    SharedData* shared_data;
    sem_t* semaphore;

    // Open the shared memory
    shm_fd = shm_open(SHM_NAME, O_RDWR, 0666);
    if (shm_fd == -1) {
        perror("shm_open");
        exit(EXIT_FAILURE);
    }

    // Map the shared memory segment to the address space of the process
    shared_data = mmap(NULL, sizeof(SharedData), PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (shared_data == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    // Open the semaphore
    semaphore = sem_open(SEM_NAME, 0);
    if (semaphore == SEM_FAILED) {
        perror("sem_open");
        exit(EXIT_FAILURE);
    }

    // Acquire the semaphore for exclusive access
    sem_wait(semaphore);

    // Read data from the shared memory
    printf("Message from shared memory: %s\n", shared_data->message);

    // Release the semaphore
    sem_post(semaphore);

    // Close the semaphore and shared memory
    sem_close(semaphore);
    shm_unlink(SHM_NAME);

    return 0;
}
