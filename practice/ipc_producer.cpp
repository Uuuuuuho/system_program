#include <iostream>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define FILENAME "data.dat"
#define DATASTRING "Now is the winter of our discontent\nMade glorious summer by this sun of York\n"

void report_and_exit(const char *msg)
{
    perror(msg);
    exit(-1);
}

int main()
{
    struct flock lock;
    lock.l_type = F_WRLCK;      // read/write (exclusive versus shared) lock
    lock.l_whence = SEEK_SET;   // base for seek offsets
    lock.l_start = 0;           // 1st byte in file
    lock.l_pid = getpid();      // process ID

    int fd;                     //file descriptor to identify a file withing a process
    if((fd = open(FILENAME, O_RDWR | O_CREAT, 0666)) < 0) report_and_exit("open failed\n");
    else
    {
        write(fd, DATASTRING, strlen(DATASTRING)); // populate data file
        std::cout << "Process " << lock.l_pid << " has written to data file...\n" << std::endl;
    }

    // now release the lock explicitly
    lock.l_type = F_ULOCK;
    if(fcntl(fd, F_SETLK, &lock) < 0) report_and_exit("explicit unlocking failed...");

    close(fd);                  // close the file: would unlock if needed
    return 0;                   // terminate the process would nolock as well
}