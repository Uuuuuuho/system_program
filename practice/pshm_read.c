/*************************************************************************\
*                  Copyright (C) Michael Kerrisk, 2019.                   *
*                                                                         *
* This program is free software. You may use, modify, and redistribute it *
* under the terms of the GNU General Public License as published by the   *
* Free Software Foundation, either version 3 or (at your option) any      *
* later version. This program is distributed without any warranty.  See   *
* the file COPYING.gpl-v3 for details.                                    *
\*************************************************************************/

/* Listing 54-3 */

/* pshm_read.c

   Usage: pshm_read shm-name

   Copy the contents of the POSIX shared memory object named in
   'name' to stdout.

   See also pshm_write.c.
*/
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "tlpi_hdr.h"
#include <time.h>
#include <semaphore.h>
int
main(int argc, char *argv[])
{
    int shm_fd;
    char *addr;
    struct stat sb;
	sem_t *sem;
	sem = sem_open("/demo",O_EXCL,O_RDWR,0);        
	int sem_val = 0;
	if(sem_getvalue(sem,&sem_val)==-1)errExit("sem_getvalue");
	while(sem_val>0){
		if(sem_wait(sem)==-1) errExit("sem_wait");
		if(sem_getvalue(sem,&sem_val)==-1)errExit("sem_getvalue");
	}
	
    shm_fd = shm_open("/demo_shm", O_RDONLY, 0);    /* Open existing object */
    if (shm_fd == -1) errExit("shm_open");
    /* Use shared memory object size as length argument for mmap()
        and as number of bytes to write() */

    if (fstat(shm_fd, &sb) == -1) errExit("fstat");

    addr = mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, shm_fd, 0);
    if (addr == MAP_FAILED) errExit("mmap");
    
    while(1){
		if(sem_post(sem)==-1) errExit("sem_post");
        sleep(2);
		if(sem_wait(sem)==-1) errExit("sem_wait");
        sleep(1);
        printf("data: %d\n", *addr);
    }

    if (close(shm_fd) == -1)  errExit("close");     /* 'shm_fd' is no longer needed */
}
