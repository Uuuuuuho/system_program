	.file	"memcpy_test.cpp"
	.section .rdata,"dr"
__ZStL19piecewise_construct:
	.space 1
.lcomm __ZStL8__ioinit,1,1
	.def	___main;	.scl	2;	.type	32;	.endef
	.text
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
LFB1741:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$32, %esp
	call	___main
	movl	$800000000, (%esp)
	call	_malloc
	movl	%eax, 20(%esp)
	movl	$800000000, (%esp)
	call	_malloc
	movl	%eax, 16(%esp)
	movl	$0, 28(%esp)
L3:
	cmpl	$199999999, 28(%esp)
	jg	L2
	movl	28(%esp), %eax
	leal	0(,%eax,4), %edx
	movl	20(%esp), %eax
	addl	%eax, %edx
	movl	28(%esp), %eax
	movl	%eax, (%edx)
	addl	$1, 28(%esp)
	jmp	L3
L2:
	movl	$0, 24(%esp)
L5:
	cmpl	$199999999, 24(%esp)
	jg	L4
	movl	24(%esp), %eax
	leal	0(,%eax,4), %edx
	movl	16(%esp), %eax
	addl	%edx, %eax
	movl	$0, (%eax)
	addl	$1, 24(%esp)
	jmp	L5
L4:
	movl	$4, 8(%esp)
	movl	$1, 4(%esp)
	movl	20(%esp), %eax
	movl	%eax, (%esp)
	call	_memset
	movl	$4, 8(%esp)
	movl	$0, 4(%esp)
	movl	16(%esp), %eax
	movl	%eax, (%esp)
	call	_memset
	movl	20(%esp), %eax
	movl	(%eax), %edx
	movl	16(%esp), %eax
	movl	%edx, (%eax)
	movl	20(%esp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	16(%esp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	$0, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1741:
	.def	___tcf_0;	.scl	3;	.type	32;	.endef
___tcf_0:
LFB2166:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$8, %esp
	movl	$__ZStL8__ioinit, %ecx
	call	__ZNSt8ios_base4InitD1Ev
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE2166:
	.def	__Z41__static_initialization_and_destruction_0ii;	.scl	3;	.type	32;	.endef
__Z41__static_initialization_and_destruction_0ii:
LFB2165:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	cmpl	$1, 8(%ebp)
	jne	L10
	cmpl	$65535, 12(%ebp)
	jne	L10
	movl	$__ZStL8__ioinit, %ecx
	call	__ZNSt8ios_base4InitC1Ev
	movl	$___tcf_0, (%esp)
	call	_atexit
L10:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE2165:
	.def	__GLOBAL__sub_I_main;	.scl	3;	.type	32;	.endef
__GLOBAL__sub_I_main:
LFB2167:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	$65535, 4(%esp)
	movl	$1, (%esp)
	call	__Z41__static_initialization_and_destruction_0ii
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE2167:
	.section	.ctors,"w"
	.align 4
	.long	__GLOBAL__sub_I_main
	.ident	"GCC: (MinGW.org GCC-6.3.0-1) 6.3.0"
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_memset;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
	.def	__ZNSt8ios_base4InitD1Ev;	.scl	2;	.type	32;	.endef
	.def	__ZNSt8ios_base4InitC1Ev;	.scl	2;	.type	32;	.endef
	.def	_atexit;	.scl	2;	.type	32;	.endef
