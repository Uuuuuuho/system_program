#include <cstdint>
#include <pthread.h>
#include <iostream>

int64_t fib(int64_t n)
{
    if (n < 2)
    {
        return n;
    }
    else
    {
        int64_t x = fib(n-1);
        int64_t y = fib(n-2);
        return x + y;
    }
}

typedef struct {
    int64_t input;
    int64_t output;
} thread_args;

void *thread_func(void *ptr)
{
    int64_t i = ((thread_args *)ptr)->input;
    ((thread_args *) ptr)->output = fib(i);
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_t thread;
    thread_args args;
    int status;
    int64_t result;

    int64_t n = 43;
    if(n < 30) result = fib(n);
    else
    {
        args.input = n-1;
        status = pthread_create(&thread,            // returned identifier for the new thread
                                NULL,               // object to set thread attribute
                                thread_func,        // routine executed after creation
                                (void*) & args);    // a single argument passed to func
        // main can continue executing
        if(status != NULL) return 1;
        result = fib(n-2);
        // wait for the thread to terminate
        status = pthread_join(thread,               // identifier of thread to wait for
                              NULL);                // terminating thread's status
        if(status != NULL) return 1;
        result += args.output;
    }
    std::cout << "Fibonacci of " << n << " is " << result << std::endl;
    return 0;
}