//
// Simple FIle System
// Student Name : YUHO CHOI
// Student Number : B315208
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/* optional */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
/***********/

#include "sfs_types.h"
#include "sfs_func.h"
#include "sfs_disk.h"
#include "sfs.h"

void dump_directory();

/* BIT operation Macros */
/* a=target variable, b=bit number to act upon 0-n */
#define BIT_SET(a,b) ((a) |= (1<<(b)))
#define BIT_CLEAR(a,b) ((a) &= ~(1<<(b)))
#define BIT_FLIP(a,b) ((a) ^= (1<<(b)))
#define BIT_CHECK(a,b) ((a) & (1<<(b)))


//===== parameters =====
#define DENTRY_MAX  SFS_DENTRYPERBLOCK * SFS_NDIRECT
#define TRUE 1
#define FALSE 0
int cpinDirEntry, cpinInodeEntry, cpinTouchErrorFlag;

static struct sfs_super spb;	// superblock
static struct sfs_dir sd_cwd = { SFS_NOINO }; // current working directory

void error_message(const char *message, const char *path, int error_code) {
	switch (error_code) {
	case -1:
		printf("%s: %s: No such file or directory\n",message, path); return;
	case -2:
		printf("%s: %s: Not a directory\n",message, path); return;
	case -3:
		printf("%s: %s: Directory full\n",message, path); return;
	case -4:
		printf("%s: %s: No block available\n",message, path); return;
	case -5:
		printf("%s: %s: Not a directory\n",message, path); return;
	case -6:
		printf("%s: %s: Already exists\n",message, path); return;
	case -7:
		printf("%s: %s: Directory not empty\n",message, path); return;
	case -8:
		printf("%s: %s: Invalid argument\n",message, path); return;
	case -9:
		printf("%s: %s: Is a directory\n",message, path); return;
	case -10:
		printf("%s: %s: Is not a file\n",message, path); return;
        case -11:
		printf("%s: %s: Target file overflow the current disk\n",message, path); return;
	default:
		printf("unknown error code\n");
		return;
	}
}

void sfs_mount(const char* path)
{
	if( sd_cwd.sfd_ino !=  SFS_NOINO )
	{
		//umount
		disk_close();
		printf("%s, unmounted\n", spb.sp_volname);
		bzero(&spb, sizeof(struct sfs_super));
		sd_cwd.sfd_ino = SFS_NOINO;
	}

	printf("Disk image: %s\n", path);

	disk_open(path);
	disk_read( &spb, SFS_SB_LOCATION );

	printf("Superblock magic: %x\n", spb.sp_magic);

	assert( spb.sp_magic == SFS_MAGIC );
	
	printf("Number of blocks: %d\n", spb.sp_nblocks);
	printf("Volume name: %s\n", spb.sp_volname);
	printf("%s, mounted\n", spb.sp_volname);
	
	sd_cwd.sfd_ino = 1;		//init at root
	sd_cwd.sfd_name[0] = '/';
	sd_cwd.sfd_name[1] = '\0';
}

void sfs_umount() {

	if( sd_cwd.sfd_ino !=  SFS_NOINO )
	{
		//umount
		disk_close();
		printf("%s, unmounted\n", spb.sp_volname);
		bzero(&spb, sizeof(struct sfs_super));
		sd_cwd.sfd_ino = SFS_NOINO;
	}
}

void sfs_touch(const char* path)
{
    
    //we assume that cwd is the root directory and root directory is empty which has . and .. only
    //unused DISK2.img satisfy these assumption
    //for new directory entry(for new file), we use cwd.sfi_direct[0] and offset 2
    //becasue cwd.sfi_directory[0] is already allocated, by .(offset 0) and ..(offset 1)
    //for new inode, we use block 6 
    // block 0: superblock,	block 1:root, 	block 2:bitmap 
    // block 3:bitmap,  	block 4:bitmap 	block 5:root.sfi_direct[0] 	block 6:unused
    //
    //if used DISK2.img is used, result is not defined
    
    struct sfs_inode ci, ni, tmp_i;    //current inode, new inode
    struct sfs_dir cd[SFS_DENTRYPERBLOCK], nd[SFS_DENTRYPERBLOCK];
    
    disk_read( &ci, sd_cwd.sfd_ino );
    
    //for consistency
    assert( ci.sfi_type == SFS_TYPE_DIR );

    int numEntry = ci.sfi_size/sizeof(struct sfs_dir);
    int i, j, newDirBlock, numDirect, newNumInode, newNumEntry;
    int exitDoubleLoop = FALSE;

    //==== check if dir already full ====
    if(numEntry < DENTRY_MAX){
        for(i= 0; i < SFS_NDIRECT; i++){
//            printf("inode checking : %d\n", i);
            if(ci.sfi_direct[i] == 0) break;
            
            disk_read( cd, ci.sfi_direct[i] );

            for(newNumEntry = 0; newNumEntry< SFS_DENTRYPERBLOCK; newNumEntry++){    //Q. should it only check itself(cd[0].sfd_name)?
//                printf("direct path name : %s\n",cd[newNumEntry].sfd_name);
                if(cd[newNumEntry].sfd_ino == 0){
                    exitDoubleLoop = TRUE;
                    break;
                }
                if(!strcmp(path,cd[newNumEntry].sfd_name)){    //same path found
            		error_message("touch",path,-6);
                    cpinTouchErrorFlag = TRUE;
        		return;
                }
            }
            
            if(exitDoubleLoop == TRUE) break;
            
        }
        
        //==== search empty dir entry ====
        for(newNumEntry = 0; newNumEntry< SFS_DENTRYPERBLOCK; newNumEntry++){
            
//            printf("newNumEntry checking : %d\n", newNumEntry);
            if(cd[newNumEntry].sfd_ino == SFS_NOINO){
                //==== empty enrty found ====
                //numDirect -= 1;
                break;
            }
        }
        
//        printf("newNumEntry in this inode : %d\n",newNumEntry);
        
        //==== lookup direct entry right before NULL ====
        for(numDirect = 0; numDirect < SFS_NDIRECT; numDirect++){
            if(ci.sfi_direct[numDirect] == 0){
                numDirect--;
                break;
            }
        }
        //==== anyway keep going ====
        //==== no same path exists ====
        //==== lookup empty block ====

        if(newNumEntry == SFS_DENTRYPERBLOCK){
            
            for(numDirect = 0; numDirect < SFS_NDIRECT; numDirect++){
                if(ci.sfi_direct[numDirect] == 0){
                    break;
                }
            }
            
            newNumInode = lookup_empty_block();
            
            if(newNumInode < 0){
                error_message("touch", path, -3);
                return;
            }
            
            cpinDirEntry = numDirect;

            bzero(&ni, sizeof(struct sfs_inode));
            ni.sfi_size = 0;
            ni.sfi_type = SFS_TYPE_FILE;

            disk_write(&ni, newNumInode);
            
            ci.sfi_size += sizeof(struct sfs_dir);
            ci.sfi_direct[numDirect] = newNumInode;
            disk_write(&ci, sd_cwd.sfd_ino);
            
        }
        
        else{
            //==== allocate a new file ====
            //==== should create error msg for overflow ====

            newNumInode = lookup_empty_block();
//            printf("newNumInode : %d\n",newNumInode);
            cpinInodeEntry = newNumEntry;
            cpinDirEntry = numDirect;

            cd[newNumEntry].sfd_ino = newNumInode;
            strncpy(cd[newNumEntry].sfd_name, path, SFS_NAMELEN);
            disk_write(cd, ci.sfi_direct[numDirect]);

            bzero(&ni, sizeof(struct sfs_inode));
            ni.sfi_size = 0;
            ni.sfi_type = SFS_TYPE_FILE;

            disk_write(&ni, newNumInode);

            
            ci.sfi_size += sizeof(struct sfs_dir);
            disk_write(&ci, sd_cwd.sfd_ino);

        }
        //===== done?? =====
    }
    
    else{   //========== directory full =========
        error_message("touch" , path, -3);
        cpinTouchErrorFlag = TRUE;
    }

}

int lookup_empty_block(){
    int numMapInBlocks = SFS_BITBLOCKS(spb.sp_nblocks), 
        numMapInBits = SFS_BITMAPSIZE(SFS_MAP_LOCATION);
    unsigned char bitmap[numMapInBlocks][numMapInBits];
    
    //    printf("numMapInBits : %d\n",numMapInBits);
    //    printf("numMapInBlocks : %d\n",numMapInBlocks);

    int i, j, k, idx;
    int numWord = SFS_BLOCKSIZE / CHAR_BIT;
    unsigned char word;


    //==== copy bitmap ====
    for(i = 0; i< numMapInBlocks; i++) disk_read(bitmap[i], SFS_MAP_LOCATION+i);

    //==== bit check step ====
    for(i = 0; i < numMapInBlocks; i++){
        for(j = 0; j < numWord; j++){
            //==== check by word ====
            word = bitmap[i][j];
            for(k = 0; k < CHAR_BIT; k++){

                if(!BIT_CHECK(word,k)){
                    BIT_SET(word, k);   //invert bit in the bit map
                    bitmap[i][j] = word;
                    disk_write(bitmap[i],SFS_MAP_LOCATION+i);
                    return (numMapInBlocks * i)+((CHAR_BIT) * (j))+(k);
               }
                
            }
        }
    }

    //========== when there is no free bree in the entire disk ==========
    return -1;
}


//==== useless ====
int current_size(){
    int numMapInBlocks = SFS_BITBLOCKS(spb.sp_nblocks), 
        numMapInBits = SFS_BITMAPSIZE(SFS_MAP_LOCATION);
    unsigned char bitmap[numMapInBlocks][numMapInBits];

    int i, j, k, cnt = 0;
    int numWord = SFS_BLOCKSIZE / CHAR_BIT;
    unsigned char word;


    //==== copy bitmap ====
    for(i = 0; i< numMapInBlocks; i++) disk_read(bitmap[i], SFS_MAP_LOCATION+i);

    //==== bit check step ====
    for(i = 0; i < numMapInBlocks; i++){
        for(j = 0; j < numWord; j++){
            //==== check by word ====
            word = bitmap[i][j];
            for(k = 0; k < CHAR_BIT; k++){

                if(!BIT_CHECK(word,k)){
//                    printf("checked\n");
//                    printf("cnt : %d\n",cnt);
                    return (spb.sp_nblocks - cnt) * SFS_BLOCKSIZE;
                }
                
                cnt++;
            }
        }
    }

    //==== when there is no free block in the entire disk ====
    cnt = 0;
//    printf("cnt : %d\n",cnt);
    return (cnt) * SFS_BLOCKSIZE;
}

void reset_bitmap(unsigned int numBlock)
{
    int numMapInBlocks = SFS_BITBLOCKS(spb.sp_nblocks), 
            numMapInBits = SFS_BITMAPSIZE(SFS_MAP_LOCATION);
    unsigned char bitmap[numMapInBlocks][numMapInBits];

    int i, j, k, idx;
    int numWord = SFS_BLOCKSIZE / CHAR_BIT;
    unsigned char word;

    
    //==== copy bitmap ====
    for(i = 0; i< numMapInBlocks; i++) disk_read(bitmap[i], SFS_MAP_LOCATION+i);

    //==== flip bitmap ====
    i = numBlock/SFS_BLOCKSIZE;
    j = numBlock / CHAR_BIT;
    k = numBlock % CHAR_BIT;
    
    word = bitmap[i][j];
    
    BIT_FLIP(word, k);
    bitmap[i][j] = word;
    
    disk_write(bitmap[i],SFS_MAP_LOCATION+i);
    
    //==== should check the result by 'fsck'====

}

void sfs_cd(const char* path)
{
    int i, j, k, l;
    struct sfs_inode c_inode, du_inode;
    struct sfs_dir dir_entry[SFS_DENTRYPERBLOCK];
    
    // dump the current directory structure
    disk_read(&c_inode, sd_cwd.sfd_ino);

    if(path == NULL) {
       sd_cwd.sfd_ino = 1;
       return;
    }

    for(j = 0; j < SFS_NDIRECT; j++) {
        if (c_inode.sfi_direct[j] == 0) break;
        disk_read(dir_entry, c_inode.sfi_direct[j]);

        
        for(i = 0; i < SFS_DENTRYPERBLOCK;i++) {
            if(!strcmp(dir_entry[i].sfd_name,path)){
//              printf("found!\n");  
                disk_read(&du_inode, dir_entry[i].sfd_ino);
                if(du_inode.sfi_type == SFS_TYPE_DIR){
                    sd_cwd.sfd_ino = dir_entry[i].sfd_ino;
                    strncpy(sd_cwd.sfd_name, dir_entry[i].sfd_name,SFS_NAMELEN);
                    printf("\n");
                    return;
                    }
                else{
                    error_message("cd", path, -2);
                    return;
                    }
//                dump_directory(&sd_cwd);
                }

            else{
                if(i == (SFS_DENTRYPERBLOCK-1)){
                    error_message("cd", path, -1);
                    printf("\n");
                    return;
                }
            }
        }
    }

}

void sfs_ls(const char* path)
{

    int i, j, k, l;
    struct sfs_inode c_inode, du_inode;
    struct sfs_dir dir_entry[SFS_DENTRYPERBLOCK];
    
    // dump the current directory structure
    disk_read(&c_inode, sd_cwd.sfd_ino);
    

    for(j = 0; j < SFS_NDIRECT; j++) {
        if (c_inode.sfi_direct[j] == 0) break;
        disk_read(dir_entry, c_inode.sfi_direct[j]);

        for(i = 0; i < SFS_DENTRYPERBLOCK;i++) {
            disk_read(&du_inode,dir_entry[i].sfd_ino);
            
                if(path == NULL){
                    if (du_inode.sfi_type == SFS_TYPE_DIR)  printf("%s/\t",  dir_entry[i].sfd_name);
                    else if(dir_entry[i].sfd_ino != SFS_NOINO) printf("%s\t",dir_entry[i].sfd_name);
                }


                
                else{
                    if(!strcmp(dir_entry[i].sfd_name,path)){
//                        printf("dir_entry[i].sfd_name : %s\n", dir_entry[i].sfd_name);
                        disk_read(&du_inode, dir_entry[i].sfd_ino);
                        
                        if (du_inode.sfi_type == SFS_TYPE_FILE)  {
                            if(strcmp(dir_entry[i].sfd_name, "f")){
                                printf("%s\t",  dir_entry[i].sfd_name);
                                printf("\n");
                                return;
                            }
                        }
                        
                        else {  //inode.type == SFS_TYPE_DIR
                            disk_read(dir_entry, du_inode.sfi_direct[0]);
                            
                            for(k = 0; k < SFS_DENTRYPERBLOCK; k++){
                                if(strcmp(dir_entry[k].sfd_name, "f")){
                                    printf("%s/\t",dir_entry[k].sfd_name);
                                }
                            }
                            



                        /****
                            for(k = 0; k < SFS_NDIRECT; k++){
                                disk_read(dir_entry, du_inode.sfi_direct[k]);
                                for(l = 0; l < SFS_DENTRYPERBLOCK; l++) 
                                    //need to ask question
                                    if(dir_entry[l].sfd_ino != SFS_NOINO)
                                        printf("%s/\t",dir_entry[l].sfd_name);
                            }
                            ****/
                            printf("\n");
                            return;
                        }
                        return;
                    }
                
                    else{
                        if(i == (SFS_DENTRYPERBLOCK-1)) {
                            error_message("ls", path, -1);
                            return;
                        }
                    }
                }



                
            }
        }
}

void sfs_mkdir(const char* org_path) 
{
    struct sfs_inode ci, ni;
    struct sfs_dir dir[SFS_DENTRYPERBLOCK], tmp[SFS_DENTRYPERBLOCK];
    int i,j, targetEntry, targetDirect, newDirBlock;

    disk_read(&ci, sd_cwd.sfd_ino);
    
    assert( ci.sfi_type == SFS_TYPE_DIR );    //not a dir

    for(targetDirect = 0; targetDirect < SFS_NDIRECT; targetDirect++){
        disk_read(dir, ci.sfi_direct[targetDirect]);
        for(targetEntry = 0; targetEntry < SFS_DENTRYPERBLOCK; targetEntry++){
            if(!strcmp(dir[targetEntry].sfd_name,org_path)){
                //==== same dir found ====
                error_message("mkdir", org_path, -6);
                return;
            }
        }
    }

    if(ci.sfi_size == SFS_BLOCKSIZE) error_message("mkdir", org_path, -3);

    for(targetDirect = 0; targetDirect < SFS_NDIRECT; targetDirect++){
         if(ci.sfi_direct[targetDirect] == 0){
            //==== empty direct found ====
            break;
        }


        disk_read(dir, ci.sfi_direct[targetDirect]);
        for(j = 0; j < SFS_DENTRYPERBLOCK; j++){
            if(dir[j].sfd_ino == SFS_NOINO){
                //==== empty DB entry found ====
                newDirBlock = lookup_empty_block();  
                dir[j].sfd_ino = newDirBlock;
                strncpy(dir[j].sfd_name, org_path, SFS_NAMELEN);

                bzero(&ni, SFS_BLOCKSIZE);
                ni.sfi_type = SFS_TYPE_DIR;
                ni.sfi_size = sizeof(struct sfs_dir);
                ni.sfi_direct[0] = lookup_empty_block();  

                //==== added ====
                disk_read(tmp,  ni.sfi_direct[0]);
                bzero(tmp, SFS_BLOCKSIZE);
                
                tmp[0].sfd_ino = newDirBlock;  //self indoe
                strncpy(tmp[0].sfd_name, ".", SFS_NAMELEN);
                tmp[1].sfd_ino = sd_cwd.sfd_ino;   //parent inode
                strncpy(tmp[1].sfd_name, "..", SFS_NAMELEN);               
                for(i = 2; i < SFS_DENTRYPERBLOCK; i++){
                    tmp[i].sfd_ino = SFS_NOINO;
                    strncpy(tmp[i].sfd_name, "", SFS_NAMELEN);
                }
                disk_write(tmp,  ni.sfi_direct[0]);
                
                //=====
                
                disk_write(&ni, newDirBlock);
                disk_write(&ci, sd_cwd.sfd_ino);
                disk_write(dir, ci.sfi_direct[targetDirect]);
                return;
            }
        }
       
    }

   //==== no empty DB entry ====
    newDirBlock = lookup_empty_block();  
    printf("newDirBlock : %d\n", newDirBlock);
    ci.sfi_direct[targetDirect] = newDirBlock;
    disk_read(tmp, ci.sfi_direct[targetDirect-1]);
    bzero(dir, SFS_BLOCKSIZE);
    dir[0].sfd_ino =  ci.sfi_direct[targetDirect];  //self indoe
    dir[1].sfd_ino = tmp[1].sfd_ino;    //parent inode
    for(i = 2; i < SFS_DENTRYPERBLOCK; i++){
        dir[i].sfd_ino = SFS_NOINO;
        strncpy(dir[i].sfd_name, "", SFS_NAMELEN);
    }

    disk_write(&ci, sd_cwd.sfd_ino);
    disk_write(dir, newDirBlock);
    
}

void sfs_rmdir(const char* org_path) 
{
    struct sfs_inode ci, ti;    //current inode, target inode
    struct sfs_dir dir[SFS_DENTRYPERBLOCK], tmp[SFS_DENTRYPERBLOCK];
    int i,j, targetEntry, targetDirect;
    int exitDoubleLoop = FALSE;
    disk_read(&ci, sd_cwd.sfd_ino);
    
    if(!strcmp(".",org_path)){
        error_message("rmdir", org_path, -8); //invalid argument
        return;
    }
    if(!strcmp("..",org_path)) {
        error_message("rmdir", org_path, -7); //invalid argument
        return;
    }
    assert( ci.sfi_type == SFS_TYPE_DIR );    //not a dir
    
    for(targetDirect = 0; targetDirect < SFS_NDIRECT; targetDirect++){
        disk_read(dir, ci.sfi_direct[targetDirect]);
        for(targetEntry = 0; targetEntry < SFS_DENTRYPERBLOCK; targetEntry++){
            if(!strcmp(dir[targetEntry].sfd_name,org_path)){
                //==== target found ====
                exitDoubleLoop = TRUE;
                break;
            }
        }
        if(exitDoubleLoop) break;
    }
    
    exitDoubleLoop= FALSE;

    if((targetDirect == SFS_NDIRECT) && (targetEntry == SFS_DENTRYPERBLOCK)) {
        error_message("rmdir", org_path, -1);    //path not exist
        return;
}

    //==== target found ====
    disk_read(&ti, dir[targetEntry].sfd_ino);
    if(ti.sfi_type == SFS_TYPE_FILE){
        error_message("rmdir", org_path, -2);
        return;
    }
    disk_read(tmp, ti.sfi_direct[0]);

    if(tmp[3].sfd_ino != SFS_NOINO)   {
        error_message("rmdir", org_path, -7);
        return;
    }
    
    //==== reset bitmap regarding to the prev numBlock ====should do hereeeeeee
    reset_bitmap(ti.sfi_direct[0]);
    reset_bitmap( dir[targetEntry].sfd_ino);

    //==== reset dir block ====
    bzero(tmp, SFS_BLOCKSIZE);
//printf("ti.sfi_direct[0] : %d\n", ti.sfi_direct[0]);

    strncpy( dir[targetEntry].sfd_name, "", SFS_NAMELEN);
     dir[targetEntry].sfd_ino = SFS_NOINO;

    //==== reset direct ====
    ci.sfi_size -= sizeof(struct sfs_dir);

    //==== disk write ====
    disk_write(tmp,ti.sfi_direct[0]);
    disk_write(dir, ci.sfi_direct[targetDirect]);
    disk_write(&ci, sd_cwd.sfd_ino);
}

void sfs_mv(const char* src_name, const char* dst_name) 
{
    struct sfs_inode ci, ti;    //current inode, target inode
    struct sfs_dir dir[SFS_DENTRYPERBLOCK], tmp[SFS_DENTRYPERBLOCK];
    int i,j, targetEntry, targetDirect;
    int exitDoubleLoop = FALSE;
    disk_read(&ci, sd_cwd.sfd_ino);

    //==== check destination name ====
    for(targetDirect = 0; targetDirect < SFS_NDIRECT; targetDirect++){
        disk_read(dir, ci.sfi_direct[targetDirect]);
        for(targetEntry = 0; targetEntry < SFS_DENTRYPERBLOCK; targetEntry++){
            if(!strcmp(dir[targetEntry].sfd_name,dst_name)){
                //==== destination already exists ====
                error_message("mv", dst_name, -6);
                return;
            }
        }
    }            

    //==== check source name ====
    for(targetDirect = 0; targetDirect < SFS_NDIRECT; targetDirect++){
        disk_read(dir, ci.sfi_direct[targetDirect]);
        for(targetEntry = 0; targetEntry < SFS_DENTRYPERBLOCK; targetEntry++){
//            printf("dir[targetEntry].sfd_name: %s, src_name : %s, dst_name : %s\n",dir[targetEntry].sfd_name,src_name,dst_name);
            if(!strcmp(dir[targetEntry].sfd_name,src_name)){
                //==== target found ====
                strncpy( dir[targetEntry].sfd_name, dst_name, SFS_NAMELEN);
                disk_write(dir,ci.sfi_direct[targetDirect]);
                return;
            }
        }
    }

    if((targetDirect == SFS_NDIRECT) && (targetEntry == SFS_DENTRYPERBLOCK)) {
        error_message("mv", src_name, -1);    //path not exist
        return;
    }

}

void sfs_rm(const char* path) 
{
    struct sfs_inode ci, ti;    //current inode, target inode
    struct sfs_dir dir[SFS_DENTRYPERBLOCK], tmp[SFS_DENTRYPERBLOCK];
    int i,j, targetEntry, targetDirect;
    int exitDoubleLoop = FALSE;
    disk_read(&ci, sd_cwd.sfd_ino);

    if(!strcmp(".",path)){
        error_message("rm", path, -8); //invalid argument
        return;
    }
    if(!strcmp("..",path)) {
        error_message("rm", path, -7); //invalid argument
        return;
    }
    assert( ci.sfi_type == SFS_TYPE_DIR);    //not a dir

    for(targetDirect = 0; targetDirect < SFS_NDIRECT; targetDirect++){
        disk_read(dir, ci.sfi_direct[targetDirect]);
        for(targetEntry = 0; targetEntry < SFS_DENTRYPERBLOCK; targetEntry++){
            if(!strcmp(dir[targetEntry].sfd_name,path)){
                //==== target found ====
                exitDoubleLoop = TRUE;
                break;
            }
        }
        if(exitDoubleLoop) break;
    }

    exitDoubleLoop= FALSE;

    if((targetDirect == SFS_NDIRECT) && (targetEntry == SFS_DENTRYPERBLOCK)) {
        error_message("rm", path, -1);    //path not exist
        return;
    }

    //==== target found ====
    disk_read(&ti, dir[targetEntry].sfd_ino);
    if(ti.sfi_type == SFS_TYPE_DIR){
        error_message("rm", path, -9);
        return;
    }
    else if(ti.sfi_type == SFS_TYPE_INVAL){
        error_message("rm", path, -10);
        return;
    }
    
    disk_read(tmp, ti.sfi_direct[0]);

    //==== reset bitmap regarding to the prev numBlock ====should do hereeeeeee
    reset_bitmap(ti.sfi_direct[0]);
    reset_bitmap( dir[targetEntry].sfd_ino);

    //==== reset dir block ====
    bzero(tmp, SFS_BLOCKSIZE);
    //printf("ti.sfi_direct[0] : %d\n", ti.sfi_direct[0]);

    strncpy( dir[targetEntry].sfd_name, "", SFS_NAMELEN);
     dir[targetEntry].sfd_ino = SFS_NOINO;

    //==== reset direct ====
    ci.sfi_size -= sizeof(struct sfs_dir);

    //==== disk write ====
    disk_write(tmp,ti.sfi_direct[0]);
    disk_write(dir, ci.sfi_direct[targetDirect]);
    disk_write(&ci, sd_cwd.sfd_ino);
}

void sfs_cpin(const char* local_path, const char* path) 
{
    int i, fRead, numCreatedInode;
    FILE *pFile = fopen(path, "rb");
    unsigned int fileSize;
    unsigned int newNumInode, newNumBlock;
    struct sfs_inode ci, ni, ini;
    struct sfs_dir dir[SFS_DENTRYPERBLOCK];

    unsigned char buf[SFS_BLOCKSIZE];
    cpinTouchErrorFlag = FALSE;

        
    //==== check file existence ====
    if(pFile == NULL) { 
        error_message("cpin",path,-1); 
        return; 
    }

    //==== check path file size ====
    fseek(pFile,0,SEEK_END);
    fileSize = ftell(pFile);
    rewind(pFile);

    //==== should figure out if this is correct ====
    unsigned int maxFileSize = SFS_BLOCKSIZE * (SFS_NDIRECT + SFS_DBPERIDB); 
//    currentDiskSize = current_size();
    
    
//    printf("currentDiskSize : %d, fileSize : %d\n",currentDiskSize, fileSize);
    
    if(maxFileSize < fileSize) { 
        error_message("cpin",path,-11); 
        fclose(pFile); 
        return; 
    }

    //==== create file ====
    sfs_touch(local_path);
    if(cpinTouchErrorFlag == TRUE) {
        error_message("cpin", local_path, -6);
        return;
    }
    
    disk_read(&ci, sd_cwd.sfd_ino);
    disk_read(dir, ci.sfi_direct[cpinDirEntry]);
    
    disk_read(&ni, dir[cpinInodeEntry].sfd_ino);    
    
            
    int dEntry = 0, iniDEntry = 0;
    
    while(1){
        fRead = fread(buf,SFS_BLOCKSIZE,1,pFile);
        

        if(dEntry < SFS_NDIRECT) {  
            //==== should resume from hereeeeeee
//            printf("direct entry fulfilling\n");
            //==== allocate a new block ====
            newNumBlock = lookup_empty_block();

            if(newNumBlock < 0){
                error_message("cpin", local_path, -4);
                return;
            }
            
            ni.sfi_direct[dEntry++] = newNumBlock;
            if(fileSize > SFS_BLOCKSIZE) {
                ni.sfi_size += SFS_BLOCKSIZE;
                fileSize -= SFS_BLOCKSIZE;
            }
            else {
                ni.sfi_size += fileSize;
                fileSize = 0;
            }
            
            
            //==== copy file ====
            disk_write(buf, newNumBlock);
            disk_write(&ni, dir[cpinInodeEntry].sfd_ino);
                
            /****
            //==== dubugging ====
            sfs_dump();
            sfs_fsck();
            //==== test ====
            return;
            ****/
        }

        else if(ni.sfi_indirect != 0){  
            //==== after allocation of indirect block ====
 //           printf("indirect entry fulfilling\n");
            newNumBlock = lookup_empty_block();
 
            if(newNumBlock < 0){
                error_message("cpin", local_path, -4);
                return;
            }
             if(fileSize > SFS_BLOCKSIZE) {
                ni.sfi_size += SFS_BLOCKSIZE;
                ini.sfi_size += SFS_BLOCKSIZE;
                fileSize -= SFS_BLOCKSIZE;
            }
            else {
                ni.sfi_size += fileSize;
                ini.sfi_size += fileSize;
                fileSize = 0;
            }
    
            ini.sfi_direct[iniDEntry++] = newNumBlock;
            
            //==== copy file ====
            
            disk_write(&ni, dir[cpinInodeEntry].sfd_ino);
            disk_write(buf, newNumBlock);
            disk_write(&ini, ni.sfi_indirect);

        }

        else{   
            //==== direct entry is full ====
//            printf("direct entries full\n");
            
            newNumInode = lookup_empty_block();
            newNumBlock = lookup_empty_block();
//            printf("newNumBlock : %d\n", newNumBlock);

            if(newNumBlock < 0){
                error_message("cpin", local_path, -4);
                return;
            }
            
            ni.sfi_indirect = newNumInode;
            
            if(fileSize > SFS_BLOCKSIZE) {
                ni.sfi_size += SFS_BLOCKSIZE;
                ini.sfi_size = SFS_BLOCKSIZE;
                fileSize -= SFS_BLOCKSIZE;
            }
            else {
                ni.sfi_size += fileSize;
                ini.sfi_size = fileSize;
                fileSize = 0;
            }
            

            //==== indirect new inode ====
            disk_read(&ini, ni.sfi_indirect);
            
            bzero(&ini, SFS_BLOCKSIZE);
            
            ini.sfi_direct[iniDEntry++] = newNumBlock;
            ini.sfi_type = SFS_TYPE_FILE;

            //==== copy file ====
            
            disk_write(&ni, dir[cpinInodeEntry].sfd_ino);
            disk_write(buf, newNumBlock);
            disk_write(&ini, ni.sfi_indirect);
        }
        
        if(fRead == 0) break;
    }
    
//    printf("copy done\n");
    //==== disk write & close file ====
    disk_read(dir,ci.sfi_direct[cpinDirEntry]);
    disk_write(&ni, dir[cpinInodeEntry].sfd_ino);
    disk_write(&ini, ni.sfi_indirect);
    fclose(pFile);
    
}

void sfs_cpout(const char* local_path, const char* path) 
{
    int i,j, fRead, numDirect, numEntry;
    int fileSize;
    struct sfs_inode ci, ti, iti;
    struct sfs_dir dir[SFS_DENTRYPERBLOCK];

    unsigned char buf[SFS_BLOCKSIZE];
    cpinTouchErrorFlag = FALSE;

    disk_read(&ci, sd_cwd.sfd_ino);

    //==== local path check ====
    for(numDirect = 0; numDirect < SFS_NDIRECT; numDirect++){
        disk_read(dir, ci.sfi_direct[numDirect]);
        for(numEntry = 0; numEntry < SFS_DENTRYPERBLOCK; numEntry++){
//            printf("dir[numEntry].sfd_name : %s\n",dir[numEntry].sfd_name);
            if(!strcmp(dir[numEntry].sfd_name, local_path)){
                //==== target found ====
                cpinTouchErrorFlag = TRUE;
                break;
            }
        }
        if(cpinTouchErrorFlag == TRUE) break;
    }
    
    if((numDirect == SFS_NDIRECT) &&(numEntry == SFS_DENTRYPERBLOCK)){
        error_message("cpout", local_path, -1);
        return;
    }

    disk_read(&ti, dir[numEntry].sfd_ino);

    if(ti.sfi_type == SFS_TYPE_DIR){
        error_message("cpout", local_path, -10);
        return;
    }
    
    FILE *pFile = fopen(path, "wb");

    fileSize = ti.sfi_size;
    printf("fileSize : %d\n", fileSize);
    
    i = 0, j = 0;
    disk_read(&iti, ti.sfi_indirect);
    
    while(fileSize > 0){
        if(i < SFS_NDIRECT){
            disk_read(buf, ti.sfi_direct[i++]);
            fwrite(buf, fileSize, 1, pFile);
            fileSize -= SFS_BLOCKSIZE;
//            printf("direct content, writing file out\n");            
        }
        else if(j == SFS_NDIRECT) break;
        else{
            disk_read(buf, iti.sfi_direct[j++]);
            fwrite(buf, fileSize, 1, pFile);
            fileSize -= SFS_BLOCKSIZE;
//            printf("indirect content, writing file out\n");            
        }
    }

    if(fileSize > 0){
        error_message("cpout", local_path, -99);
        return;
    }
//    printf("cpout done\n");
    fclose(pFile);
    return;
}

void dump_inode(struct sfs_inode inode) {
	int i;
	struct sfs_dir dir_entry[SFS_DENTRYPERBLOCK];

	printf("size %d type %d direct ", inode.sfi_size, inode.sfi_type);
	for(i=0; i < SFS_NDIRECT; i++) {
		printf(" %d ", inode.sfi_direct[i]);
	}
	printf(" indirect %d",inode.sfi_indirect);
	printf("\n");

	if (inode.sfi_type == SFS_TYPE_DIR) {
		for(i=0; i < SFS_NDIRECT; i++) {
			if (inode.sfi_direct[i] == 0) break;
			disk_read(dir_entry, inode.sfi_direct[i]);
			dump_directory(dir_entry);
		}
	}

}

void dump_directory(struct sfs_dir dir_entry[]) {
	int i;
	struct sfs_inode inode;
	for(i=0; i < SFS_DENTRYPERBLOCK;i++) {
		printf("%d %s\n",dir_entry[i].sfd_ino, dir_entry[i].sfd_name);
		disk_read(&inode,dir_entry[i].sfd_ino);
		if (inode.sfi_type == SFS_TYPE_FILE) {
			printf("\t");
			dump_inode(inode);
		}
	}
}

void sfs_dump() {
	// dump the current directory structure
	struct sfs_inode c_inode;

	disk_read(&c_inode, sd_cwd.sfd_ino);
	printf("cwd inode %d name %s\n",sd_cwd.sfd_ino,sd_cwd.sfd_name);
	dump_inode(c_inode);
	printf("\n");

}
