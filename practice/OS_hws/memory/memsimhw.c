#if 1
#endif
//
// Virual Memory Simulator Homework
// One-level page table system with FIFO and LRU
// Two-level page table system with LRU
// Inverted page table with a hashing system
// Submission Year:2019
// Student Name:Choi Yuho
// Student Number:B315208
//
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#pragma warning(disable:4996)

#define PAGESIZEBITS 12                 // page size = 4Kbytes
#define VIRTUALADDRBITS 32              // virtual address space size = 4Gbytes
#define TRACESIZE 1000000
#define TRUE 1
#define FALSE 0

typedef enum _SIM_TYPE {
	ONE_LEVEL_PT = 0,
	TWO_LEVEL_PT = 1,
	INVERTED_PT = 2,
	ALL = 3
}SIM_TYPE;

typedef enum _FIFO_LRU{
    FIFO = 0,
    LRU = 1
}FIFO_LRU;

struct pageTableEntry {
	int level;                              // page table level (1 or 2)
	char valid;
	struct pageTableEntry* secondLevelPageTable;    // valid if this entry is for the first level page table (level = 1)
	int frameNumber;                                                                // valid if this entry is for the second level page table (level = 2)
};

struct framePage {
	int number;                     // frame number
	int pid;                        // Process id that owns the frame
	int virtualPageNumber;                  // virtual page number using the frame
	struct framePage* lruLeft;
	struct framePage* lruRight; // for LRU circular doubly linked list
};

struct invertedPageTableEntry {
	int pid;                                        // process id
	int virtualPageNumber;          // virtual page number
	int frameNumber;                        // frame number allocated
	struct invertedPageTableEntry* next;
};

struct procEntry {
	char* traceName;                        // the memory trace name
	int pid;                                        // process (trace) id
	int ntraces;                            // the number of memory traces
	int num2ndLevelPageTable;       // The 2nd level page created(allocated);
	int numIHTConflictAccess;       // The number of Inverted Hash Table Conflict Accesses
	int numIHTNULLAccess;           // The number of Empty Inverted Hash Table Accesses
	int numIHTNonNULLAcess;         // The number of Non Empty Inverted Hash Table Accesses
	int numPageFault;                       // The number of page faults
	int numPageHit;                         // The number of page hits
	struct pageTableEntry* firstLevelPageTable;
	FILE* tracefp;
};

struct virtualAddress {
	unsigned int addr;
	unsigned int vpn;
	unsigned int first_vpn;
	unsigned int second_vpn;
	unsigned int off;
	char rw;
};

struct physicalAddress {
	unsigned int addr;
};

struct HashLUT {
	unsigned int idx;
};

struct framePage* oldestFrame; // the oldest frame pointer

int firstLevelBits, secondLevelBits, phyMemSizeBits, numProcess, 
    IHT_idx, oldest_IHT_idx, tmp_numFrame;
int s_flag = 0, simType = 0, curSimType = 0;	//simulator Type
unsigned int offset = 0x00000FFF, NofPTE;
struct procEntry* procTable;
struct pageTableEntry *PT, *tmpPT;
struct invertedPageTableEntry *IHT, *tmpIPTE, *newIPTE;
struct virtualAddress VA, tmpVA;
struct physicalAddress PA;
struct framePage* phyMem;
int nFrame, curFrame;
FIFO_LRU FIFO_LRU_flag;
unsigned int num1stPTE, num2ndPTE;

int queue = 0;

void initPT(struct pageTableEntry* PT, int numPTE);
void initSecondPT(struct pageTableEntry** PT, int numFirstPT, int numSecondPT);
void initIPT(struct invertedPageTableEntry* IPT, int nFrame);
void initPhyMem(struct framePage* phyMem, int nFrame);
void VA2vpnNoff();
void initProcTable(struct procEntry procTable[], int numProcess, int offset, char** argv);
void initHashLUT(struct HashLUT** LUT, unsigned int numProcess, unsigned int NofPTE, unsigned int nFrame);
void LRUhit(struct framePage* phyMem);
void print_LRU_queue(struct framePage * Left);

void oneLevelVMSim_FIFO(void);
void oneLevelVMSim_LRU(void);
void accessFirstPT(int curProc);

void twoLevelVMSim_LRU(void);
void accessSecondPT(int curProc);
void twoLevelPageFault(int curProc);

void InvertedSim_LRU(void);
void accessIHT(int curProc);
int searchIHT(struct invertedPageTableEntry* cur_IPTE, int pid, int vpn);
void deleteIPTE(struct invertedPageTableEntry* cur_IPTE,  int pid, int vpn);
void insertIPTE(struct invertedPageTableEntry* oldIPTE, int pid, int vpn);
void print_IPT_queue(struct invertedPageTableEntry *IPT);


void paraConfig(int argc, char* arg[]);


int main(int argc, char* argv[]) {
    int current_proc = 0;
	num1stPTE = 0, num2ndPTE = 0;
	

	paraConfig(argc, argv);
	//printf("numProcess : %d\n", numProcess);

	//assign physical MM 
	NofPTE = 1 << (VIRTUALADDRBITS - PAGESIZEBITS);
	num1stPTE = 1 << firstLevelBits;
	num2ndPTE = 1 << secondLevelBits;

	// initialize procTable for memory simulations

	nFrame = (1 << (phyMemSizeBits - PAGESIZEBITS)); assert(nFrame > 0);
	phyMem = (struct framePage*)malloc(sizeof(struct framePage) * nFrame);
    
    
	printf("\nNum of Frames %d Physical Memory Size %ld bytes\n", nFrame, (1 << phyMemSizeBits));
    

	procTable = (struct procEntry*)malloc(sizeof(struct procEntry) * numProcess);
	(s_flag == TRUE) ? initProcTable(procTable, numProcess, 5, argv) : initProcTable(procTable, numProcess, 4, argv);

	for (current_proc = 0; current_proc < numProcess; current_proc++) printf("process %d opening %s\n", procTable[current_proc].pid, procTable[current_proc].traceName);

	switch (simType) {
	case(ONE_LEVEL_PT):
        curSimType = ONE_LEVEL_PT;
		oneLevelVMSim_FIFO();

        //clear history
        (s_flag == TRUE) ? initProcTable(procTable, numProcess, 5, argv) : initProcTable(procTable, numProcess, 4, argv);
        for (current_proc = 0; current_proc < numProcess; current_proc++) rewind(procTable[current_proc].tracefp);

		oneLevelVMSim_LRU();

		break;
	case(TWO_LEVEL_PT):
        curSimType = TWO_LEVEL_PT;

        twoLevelVMSim_LRU();


		break;
		
	case(INVERTED_PT):
        curSimType = INVERTED_PT;

        InvertedSim_LRU();
		break;
	case(ALL):	//case of 'ALL'

		break;
	default:
		break;
	}
	
	return(0);
}

void initPT(struct pageTableEntry* PT, int numPTE) {
	int i;
	for (i = 0; i < numPTE; i++) {
		PT[i].level = 1;
		PT[i].valid = 0;
		PT[i].secondLevelPageTable = NULL;
		PT[i].frameNumber = -1;
	}
}

void initSecondPT(struct pageTableEntry** PT, int numFirstPT, int numSecondPT) {
	int i, j;
	//printf("initSecondPT started\n");
	for (i = 0; i < numFirstPT; i++)
		for (j = 0; j < numSecondPT; j++) {
			PT[i][j].level = 1;
			PT[i][j].valid = FALSE;
			PT[i][j].secondLevelPageTable = NULL;
			PT[i][j].frameNumber = -1;
		}
}

void initIPT(struct invertedPageTableEntry* IPT, int nFrame) {
	int i;
	//printf("initIPT started\n");
	for (i = 0; i < nFrame; i++) {
		IPT[i].frameNumber = -1;
		IPT[i].next = NULL;
		IPT[i].pid = -1;
		IPT[i].virtualPageNumber = -1;
	}
}

void initPhyMem(struct framePage* phyMem, int nFrame) {
	int i;
	for (i = 0; i < nFrame; i++) {
		phyMem[i].number = i;
		phyMem[i].pid = -1;
		phyMem[i].virtualPageNumber = -1;
		phyMem[i].lruLeft = &phyMem[(i - 1 + nFrame) % nFrame];
		phyMem[i].lruRight = &phyMem[(i + 1 + nFrame) % nFrame];
	}

	oldestFrame = &phyMem[0];

}

void VA2vpnNoff() {
	int i;
	unsigned int mask = 0;
	for (i = 0; i < secondLevelBits; i++)	mask = (mask << 1) | 1;

	VA.off = (offset & VA.addr);
	VA.vpn = (VA.addr >> PAGESIZEBITS);
	VA.first_vpn = (VA.vpn >> (secondLevelBits));
	VA.second_vpn = (VA.vpn & mask);
}

void tmpVA2vpnNoff() {
	int i;
	unsigned int mask = 0;
	for (i = 0; i < secondLevelBits; i++)	mask = (mask << 1) | 1;

	tmpVA.off = (offset & oldestFrame->virtualPageNumber);
	tmpVA.vpn = (oldestFrame->virtualPageNumber >> PAGESIZEBITS);
	tmpVA.first_vpn = (oldestFrame->virtualPageNumber>> (secondLevelBits));
	tmpVA.second_vpn = (oldestFrame->virtualPageNumber & mask);
}

void initProcTable(struct procEntry procTable[], int numProcess, int offset, char** argv) {
	int i, j;
	for (i = 0; i < numProcess; i++) {
		procTable[i].traceName = argv[i + offset];
		procTable[i].pid = i;
		procTable[i].ntraces = 0;
		procTable[i].num2ndLevelPageTable = 0;
		procTable[i].numIHTConflictAccess = 0;
		procTable[i].numIHTNULLAccess = 0;
		procTable[i].numIHTNonNULLAcess = 0;
		procTable[i].numPageFault = 0;
		procTable[i].numPageHit = 0;
		procTable[i].firstLevelPageTable = NULL;
		procTable[i].tracefp = fopen(argv[i + offset], "r");
	}
}

void initHashLUT(struct HashLUT** LUT, unsigned int numProcess, unsigned int NofPTE, unsigned int nFrame) {
	int pid, vpn;
	for (pid = 0; pid < numProcess; pid++)
		for (vpn = 0; vpn < NofPTE; vpn++) {
			LUT[pid][vpn].idx = (pid + vpn) % nFrame;
		}
}

void LRUhit(struct framePage* phyMem) {
	(phyMem->lruRight)->lruLeft = (phyMem->lruLeft);
	(phyMem->lruLeft)->lruRight = (phyMem->lruRight);

	phyMem->lruLeft = oldestFrame;
	phyMem->lruRight = oldestFrame->lruRight;

	(oldestFrame->lruRight)->lruLeft = phyMem;
	oldestFrame->lruRight = phyMem;
};


void print_LRU_queue(struct framePage * Left){
//printf("queue : %d\n",queue);
    printf("%d\t",Left->number);
    queue++;
    if(queue < 8) {print_LRU_queue(Left->lruRight);queue++;}
    else printf("\n");
}

void accessFirstPT(int curProc){
    //create PT
    if(procTable[curProc].firstLevelPageTable == NULL){
        tmpPT = (struct pageTableEntry*)malloc(sizeof(struct pageTableEntry) * NofPTE);
        initPT(tmpPT, NofPTE);
        procTable[curProc].firstLevelPageTable = tmpPT;
    }

	//PTE NOT MAPPED YET    
    if (procTable[curProc].firstLevelPageTable[VA.vpn].valid == FALSE) {
		procTable[curProc].numPageFault++;

        //no frame in the PTE
        if(oldestFrame->pid < 0){
            procTable[curProc].firstLevelPageTable[VA.vpn].valid = TRUE;
			procTable[curProc].firstLevelPageTable[VA.vpn].frameNumber = oldestFrame->number;

			phyMem[oldestFrame->number].virtualPageNumber = VA.vpn;
			phyMem[oldestFrame->number].pid = curProc;

        }
            

        //page fault & MM is full
		else {	
            procTable[oldestFrame->pid].firstLevelPageTable[oldestFrame->virtualPageNumber].valid = FALSE;
		    procTable[curProc].firstLevelPageTable[VA.vpn].valid = TRUE;
			procTable[curProc].firstLevelPageTable[VA.vpn].frameNumber = oldestFrame->number;
            
			phyMem[procTable[curProc].firstLevelPageTable[VA.vpn].frameNumber].virtualPageNumber = VA.vpn;
			phyMem[procTable[curProc].firstLevelPageTable[VA.vpn].frameNumber].pid = curProc;
		}
        oldestFrame = oldestFrame->lruLeft;
        
	}

    //page hit
	else { 
            procTable[curProc].numPageHit++;
        
            if(FIFO_LRU_flag == FIFO);
            else{   //LRU PT hit
                if (oldestFrame->lruRight == &phyMem[procTable[curProc].firstLevelPageTable[VA.vpn].frameNumber]);
    			else if (oldestFrame == &phyMem[procTable[curProc].firstLevelPageTable[VA.vpn].frameNumber]) oldestFrame = oldestFrame->lruLeft;
	    		else LRUhit(&phyMem[procTable[curProc].firstLevelPageTable[VA.vpn].frameNumber]);
            }
	}
	

	PA.addr = procTable[curProc].firstLevelPageTable[VA.vpn].frameNumber << PAGESIZEBITS;
	PA.addr = PA.addr | VA.off;


	if (s_flag) {
		// -s option print statement
		printf("One-Level procID %d traceNumber %d virtual addr %x physical addr %x\n", procTable->pid, procTable->ntraces, VA.vpn, PA.addr);

	}


}

void oneLevelVMSim_FIFO() {
	int current_proc;
    
    printf("=============================================================\n");
    printf("The One-Level Page Table with FIFO Memory Simulation Starts .....\n");
    printf("=============================================================\n");

    //=============initialization==========
    curFrame = 0;
    FIFO_LRU_flag = FIFO;
    //=====================================
    
	initPhyMem(phyMem, nFrame);

	for (; procTable[numProcess - 1].ntraces < TRACESIZE;) {
		for (current_proc = 0; current_proc < numProcess; current_proc++) {
            

			fscanf(procTable[current_proc].tracefp, "%x %c\n", &VA.addr, &VA.rw);
			VA2vpnNoff();

			accessFirstPT(current_proc);
			procTable[current_proc].ntraces++;
		}
	}
	
	for (current_proc = 0; current_proc < numProcess; current_proc++) {
		printf("**** %s *****\n", procTable[current_proc].traceName);
		printf("Proc %d Num of traces %d\n", procTable[current_proc].pid, procTable[current_proc].ntraces);
		printf("Proc %d Num of Page Faults %d\n", procTable[current_proc].pid, procTable[current_proc].numPageFault);
		printf("Proc %d Num of Page Hit %d\n", procTable[current_proc].pid, procTable[current_proc].numPageHit);
		assert(procTable[current_proc].numPageHit + procTable[current_proc].numPageFault == procTable[current_proc].ntraces);
	}

}

void oneLevelVMSim_LRU() {
	int current_proc;
    printf("=============================================================\n");
    printf("The One-Level Page Table with LRU Memory Simulation Starts .....\n");
    printf("=============================================================\n");

    //=============initialization==========
    curFrame = 0;
    FIFO_LRU_flag = LRU;
    //=====================================

	initPhyMem(phyMem, nFrame);

	for (; procTable[numProcess - 1].ntraces < TRACESIZE;) {
		for (current_proc = 0; current_proc < numProcess; current_proc++) {
            

			fscanf(procTable[current_proc].tracefp, "%x %c\n", &VA.addr, &VA.rw);
			VA2vpnNoff();

			accessFirstPT(current_proc);
			procTable[current_proc].ntraces++;
		}
	}
	
	for (current_proc = 0; current_proc < numProcess; current_proc++) {
		printf("**** %s *****\n", procTable[current_proc].traceName);
		printf("Proc %d Num of traces %d\n", procTable[current_proc].pid, procTable[current_proc].ntraces);
		printf("Proc %d Num of Page Faults %d\n", procTable[current_proc].pid, procTable[current_proc].numPageFault);
		printf("Proc %d Num of Page Hit %d\n", procTable[current_proc].pid, procTable[current_proc].numPageHit);
		assert(procTable[current_proc].numPageHit + procTable[current_proc].numPageFault == procTable[current_proc].ntraces);
	}

}

//LRU two-level-PT
void accessSecondPT(int curProc){
    if(procTable[curProc].firstLevelPageTable == NULL){
        tmpPT = (struct pageTableEntry*)malloc(sizeof(struct pageTableEntry) * num1stPTE);
        initPT(tmpPT, num1stPTE);
        procTable[curProc].firstLevelPageTable = tmpPT;
    }

    //NO SECOND PT CREATED YET
    if(procTable[curProc].firstLevelPageTable[VA.first_vpn].secondLevelPageTable == NULL){  
        procTable[curProc].num2ndLevelPageTable++;
        
        tmpPT = (struct pageTableEntry*)malloc(sizeof(struct pageTableEntry) * num2ndPTE);
        initPT(tmpPT, num2ndPTE);
        
        procTable[curProc].firstLevelPageTable[VA.first_vpn].secondLevelPageTable = tmpPT;
    }

    //page hit
    if(procTable[curProc].firstLevelPageTable[VA.first_vpn].secondLevelPageTable[VA.second_vpn].valid == TRUE){

        procTable[curProc].numPageHit++;
    
        if (oldestFrame->lruRight == &phyMem[procTable[curProc].firstLevelPageTable[VA.first_vpn].secondLevelPageTable[VA.second_vpn].frameNumber]);
        else if (oldestFrame == &phyMem[procTable[curProc].firstLevelPageTable[VA.first_vpn].secondLevelPageTable[VA.second_vpn].frameNumber]) oldestFrame = oldestFrame->lruLeft;
        else LRUhit(&phyMem[procTable[curProc].firstLevelPageTable[VA.first_vpn].secondLevelPageTable[VA.second_vpn].frameNumber]);

    }
    
    else{
        //page fault
        procTable[curProc].numPageFault++;

        //no frame in the PTE
        if(oldestFrame->pid < 0){
            procTable[curProc].firstLevelPageTable[VA.first_vpn].secondLevelPageTable[VA.second_vpn].valid = TRUE;
            procTable[curProc].firstLevelPageTable[VA.first_vpn].secondLevelPageTable[VA.second_vpn].frameNumber = oldestFrame->number;

            phyMem[oldestFrame->number].virtualPageNumber = VA.vpn;
            phyMem[oldestFrame->number].pid = curProc;
            
            oldestFrame = oldestFrame->lruLeft;
        }
            
        //secondPT exist
        else{   
            twoLevelPageFault(curProc);
        }
    }
    
	PA.addr = procTable[curProc].firstLevelPageTable[VA.first_vpn].secondLevelPageTable[VA.second_vpn].frameNumber << PAGESIZEBITS;
	PA.addr = PA.addr | VA.off;


	if (s_flag) {
		// -s option print statement
		printf("Two-Level procID %d traceNumber %d virtual addr %x physical addr %x\n", procTable[curProc].pid, procTable[curProc].ntraces, VA.vpn, PA.addr);

	}

}

void twoLevelPageFault(int curProc){
    tmpVA2vpnNoff();
            
    procTable[oldestFrame->pid].firstLevelPageTable[tmpVA.first_vpn].secondLevelPageTable[tmpVA.second_vpn].valid = FALSE;
    
    procTable[curProc].firstLevelPageTable[VA.first_vpn].secondLevelPageTable[VA.second_vpn].valid = TRUE;
    procTable[curProc].firstLevelPageTable[VA.first_vpn].secondLevelPageTable[VA.second_vpn].frameNumber = oldestFrame->number;

	phyMem[oldestFrame->number].virtualPageNumber = VA.vpn;
	phyMem[oldestFrame->number].pid = curProc;

    oldestFrame = oldestFrame->lruLeft;
}

    
void twoLevelVMSim_LRU(){
    int current_proc;
    
    
    printf("=============================================================\n");
    printf("The Two-Level Page Table Memory Simulation Starts .....\n");
    printf("=============================================================\n");

    //=============initialization==========
    curFrame = 0;
    FIFO_LRU_flag = LRU;
    //=====================================

	initPhyMem(phyMem, nFrame);

	for (; procTable[numProcess - 1].ntraces < TRACESIZE;) {
		for (current_proc = 0; current_proc < numProcess; current_proc++) {

			fscanf(procTable[current_proc].tracefp, "%x %c\n", &VA.addr, &VA.rw);
			VA2vpnNoff();

			accessSecondPT(current_proc);
			procTable[current_proc].ntraces++;
		}
	}
    
	for (current_proc = 0; current_proc < numProcess; current_proc++) {
		printf("**** %s *****\n", procTable[current_proc].traceName);
		printf("Proc %d Num of traces %d\n", procTable[current_proc].pid, procTable[current_proc].ntraces);
        printf("Proc %d Num of second level page tables aloocated %d\n",procTable[current_proc].pid, procTable[current_proc].num2ndLevelPageTable);
		printf("Proc %d Num of Page Faults %d\n", procTable[current_proc].pid, procTable[current_proc].numPageFault);
		printf("Proc %d Num of Page Hit %d\n", procTable[current_proc].pid, procTable[current_proc].numPageHit);
		assert(procTable[current_proc].numPageHit + procTable[current_proc].numPageFault == procTable[current_proc].ntraces);
	}
}

void InvertedSim_LRU(void){
    printf("=============================================================\n");
	printf("The Inverted Page Table Memory Simulation Starts .....\n");
	printf("=============================================================\n");

    int current_proc;
    
    //=============initialization==========
    curFrame = 0;
    FIFO_LRU_flag = LRU;
    //=====================================

	initPhyMem(phyMem, nFrame);
    
    //create IHT
    IHT = (struct invertedPageTableEntry*)malloc(sizeof(struct invertedPageTableEntry) * nFrame);
    initIPT(IHT, nFrame);
    
	for (; procTable[numProcess - 1].ntraces < TRACESIZE;) {
		for (current_proc = 0; current_proc < numProcess; current_proc++) {

			fscanf(procTable[current_proc].tracefp, "%x %c\n", &VA.addr, &VA.rw);
			VA2vpnNoff();
			accessIHT(current_proc);
			procTable[current_proc].ntraces++;
		}
	}

	for (current_proc = 0; current_proc < numProcess; current_proc++) {
        printf("**** %s *****\n", procTable[current_proc].traceName);
    	printf("Proc %d Num of traces %d\n", procTable[current_proc].pid, procTable[current_proc].ntraces);
    	printf("Proc %d Num of Inverted Hash Table Access Conflicts %d\n", procTable[current_proc].pid, procTable[current_proc].numIHTConflictAccess);
    	printf("Proc %d Num of Empty Inverted Hash Table Access %d\n", procTable[current_proc].pid, procTable[current_proc].numIHTNULLAccess);
    	printf("Proc %d Num of Non-Empty Inverted Hash Table Access %d\n", procTable[current_proc].pid, procTable[current_proc].numIHTNonNULLAcess);
    	printf("Proc %d Num of Page Faults %d\n", procTable[current_proc].pid, procTable[current_proc].numPageFault);
    	printf("Proc %d Num of Page Hit %d\n", procTable[current_proc].pid, procTable[current_proc].numPageHit);
    	assert(procTable[current_proc].numPageHit + procTable[current_proc].numPageFault == procTable[current_proc].ntraces);
    	//assert(procTable[current_proc].numIHTNULLAccess + procTable[current_proc].numIHTNonNULLAcess == procTable[current_proc].ntraces);
       }
}

void accessIHT(int curProc){
    int i;
 //       printf("vpn : %x\n", VA.vpn);
 //       for(i = 0; i < nFrame; i++) print_IPT_queue(&IHT[i]);
    //    sleep(1);

    //IHT index calculation
	IHT_idx = (curProc + VA.vpn) % nFrame;
    oldest_IHT_idx = (oldestFrame->pid + oldestFrame->virtualPageNumber) % nFrame;
//    printf("IHT_idx : %d\n", IHT_idx);
    tmp_numFrame = searchIHT(&IHT[IHT_idx],curProc, VA.vpn);
//printf("tmp_numFrame : %d\n", tmp_numFrame);
	//search fail    
	if(tmp_numFrame < 0){
        procTable[curProc].numPageFault++;
        

        insertIPTE(&IHT[IHT_idx], curProc, VA.vpn);

    	if (oldestFrame->pid >= 0) {
//            printf("old vpn : %x\n", oldestFrame->virtualPageNumber);
             if((IHT[oldest_IHT_idx].pid == oldestFrame->pid) && (IHT[oldest_IHT_idx].virtualPageNumber == oldestFrame->virtualPageNumber)){
                if(IHT[oldest_IHT_idx].next == NULL) initIPT(&IHT[oldest_IHT_idx],1);
                else {
                    IHT[oldest_IHT_idx].virtualPageNumber = IHT[oldest_IHT_idx].next->virtualPageNumber;
                    IHT[oldest_IHT_idx].frameNumber = IHT[oldest_IHT_idx].next->frameNumber;
                    IHT[oldest_IHT_idx].pid = IHT[oldest_IHT_idx].next->pid;
                    IHT[oldest_IHT_idx].next = IHT[oldest_IHT_idx].next->next;
                }
             }
             else deleteIPTE(&IHT[oldest_IHT_idx], oldestFrame->pid, oldestFrame->virtualPageNumber);
    	}

        phyMem[oldestFrame->number].virtualPageNumber = VA.vpn;
        phyMem[oldestFrame->number].pid = curProc;

        oldestFrame = oldestFrame->lruLeft;                
    }
	
	//search success
    else{
         //page hit
        procTable[curProc].numPageHit++;
	//	printf("tmp_numFrame : %d\t", tmp_numFrame);
		//LRU policy
        if (oldestFrame->lruRight == &phyMem[tmp_numFrame]);
    	else if (oldestFrame == &phyMem[tmp_numFrame]) oldestFrame = oldestFrame->lruLeft;
    	else LRUhit(&phyMem[tmp_numFrame]);
    }
	//printf("oldest frame # : %d\n",oldestFrame->number);
	
}

int searchIHT(struct invertedPageTableEntry *cur_IPTE, int pid, int vpn){
//	printf("problem in searchIPTE\n");
//	printf("cur pid : %d, vpn : %x\n",cur_IPTE->pid, cur_IPTE->virtualPageNumber);

    //try and fix this if there is any error
//    printf("cur_vpn : %d\n", cur_IPTE->virtualPageNumber);
//    printf("cur_numFrame : %d\n", cur_IPTE->frameNumber);
    if(cur_IPTE->pid >= 0) procTable[pid].numIHTNonNULLAcess++;

    if(cur_IPTE->virtualPageNumber == vpn){
        return cur_IPTE->frameNumber;
        }
    else if(cur_IPTE->next != NULL){
        searchIHT(cur_IPTE->next, pid, vpn);
    }
    else {
        return -1;
    }
}

void deleteIPTE(struct invertedPageTableEntry* cur_IPTE, int pid, int vpn){
	//printf("cur pid : %d, vpn : %x\n",cur_IPTE->pid, cur_IPTE->virtualPageNumber);

//	printf("problem in deleteIPTE\n");
    //delete IPTE node only when there is the target node surely in the list
    //try and fix this if there is any error
    
    if(((cur_IPTE->next)->pid == pid) && ((cur_IPTE->next)->virtualPageNumber == vpn)){
        cur_IPTE->next = (cur_IPTE->next)->next;
    }
    
    else
        deleteIPTE(cur_IPTE->next, pid, vpn);
}


void insertIPTE(struct invertedPageTableEntry* oldIPTE, int pid, int vpn){
//	printf("problem in insertIPTE\n");
    //try and fix this if there is any error

	if(oldIPTE->pid >= 0){
   //     printf("non-NULL case\n");
      //  printf("IHT pid : %d, vpn : %x\n", IHT[IHT_idx].pid, IHT[IHT_idx].virtualPageNumber);
        newIPTE = (struct invertedPageTableEntry*)malloc(sizeof(struct invertedPageTableEntry));

		newIPTE->virtualPageNumber = oldIPTE->virtualPageNumber;
		newIPTE->pid = oldIPTE->pid;
		newIPTE->frameNumber = oldIPTE->frameNumber;
		newIPTE->next = oldIPTE->next;
//        printf("new pid : ");
//        print_IPT_queue(newIPTE);
//		printf("new pid : %d\tvpn : %x\n", newIPTE->pid, newIPTE->virtualPageNumber);
		oldIPTE->next = newIPTE;
	}
    else procTable[pid].numIHTNULLAccess++;
    oldIPTE->virtualPageNumber = vpn;
    oldIPTE->pid = pid;
    oldIPTE->frameNumber = oldestFrame->number;
   	

    
//    printf("IHT pid : %d, vpn : %x\n", IHT[IHT_idx].pid, IHT[IHT_idx].virtualPageNumber);
}

void print_IPT_queue(struct invertedPageTableEntry *IPT){
    
    printf("%x\t", IPT->virtualPageNumber);
        if(IPT->next != NULL){
                print_IPT_queue(IPT->next);
        }
        else printf("\n");
}


void paraConfig(int argc, char* arg[]) {
	if (!strcmp(arg[1], "-s")) {	//when s_flag = TRUE
		s_flag = TRUE;
		switch (atoi(arg[2])) {
		case 0:
			simType = ONE_LEVEL_PT;
			break;
		case 1:
			simType = TWO_LEVEL_PT;
			break;
		case 2:
			simType = INVERTED_PT;
			break;
		default:
			simType = ALL;
			break;
		}//check this later..
		firstLevelBits = atoi(arg[3]);
		secondLevelBits = VIRTUALADDRBITS - firstLevelBits - PAGESIZEBITS;
		phyMemSizeBits = atoi(arg[4]);
		numProcess = argc - 5;
	}
	else {
		switch (atoi(arg[1])) {
		case 0:
			simType = ONE_LEVEL_PT;
			break;
		case 1:
			simType = TWO_LEVEL_PT;
			break;
		case 2:
			simType = INVERTED_PT;
			break;
		default:
			simType = ALL;
			break;
		}
		firstLevelBits = atoi(arg[2]);
		secondLevelBits = VIRTUALADDRBITS - firstLevelBits - PAGESIZEBITS;
		phyMemSizeBits = atoi(arg[3]);
		numProcess = argc - 4;
	}

	if (argc == 1) {

		printf("Usage : %s [-s] firstLevelBits PhysicalMemorySizeBits TraceFileNames\n", arg[0]); exit(1);
	}

	if (phyMemSizeBits < PAGESIZEBITS) {
		printf("PhysicalMemorySizeBits %d should be larger than PageSizeBits %d\n", phyMemSizeBits, PAGESIZEBITS); exit(1);
	}
	if (VIRTUALADDRBITS - PAGESIZEBITS - firstLevelBits <= 0) {
		printf("firstLevelBits %d is too Big for the 2nd level page system\n", firstLevelBits); exit(1);
	}


}




