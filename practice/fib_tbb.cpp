#include <cstdint>
#include <iostream>
#include "tbb/task.h"

using namespace tbb;
class FibTask: public task 
{
public:
    const int64_t n;
    int64_t* const sum;
    FibTask(int64_t n_, int64_t* sum_):
            n(n_), sum(sum_) {}
    
    task* execute()
    {
        if(n<2) *sum = n;
        else
        {
            int64_t x, y;
            FibTask& a = *new(allocate_child())
                            FibTask(n-1, &x);
            FibTask& b = *new(allocate_child())
                            FibTask(n-2, &y);
            set_ref_count(3);
            spawn(b);
            spawn_and_wait_for_all(a);
            *sum = x + y;
        }
        return NULL;
    }
};

int main(int argc, char *argv[])
{
    int64_t res;
    int64_t n = 43;
    FibTask &a = *new(task::allocate_root())
                        FibTask(n, &res);
    task::spawn_root_and_wait(a);

    std::cout << "Fibonacci of " << n << " is " << res << std::endl;
    return 0;
}