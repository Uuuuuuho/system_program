#include <iostream>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#define FILENAME "data.dat"

void report_and_exit(const char *msg)
{
    perror(msg);
    exit(-1);
}

int main()
{
    struct flock lock;
    lock.l_type = F_WRLCK;      // read/write (exclusive versus shared) lock
    lock.l_whence = SEEK_SET;   // base for seek offsets
    lock.l_start = 0;           // 1st byte in file
    lock.l_len = 0;             // '0' here means 'untile EOF'
    lock.l_pid = getpid();      // process ID

    int fd;                     // file descriptor to identify a file withing a process
    if((fd = open(FILENAME, O_RDONLY)) < 0) report_and_exit("open to read failed...");
    fcntl(fd, F_GETLK, &lock);  // sets lock.l_type to F_UNLOCK if no write lock
    if(lock.l_type != F_UNLCK) report_and_exit("file is still write locked...");

    lock.l_type = F_RDLCK;      // prevents any writing during the reading
    if(fcntl(fd, F_SETLK, &lock) < 0) report_and_exit("can't get a read-only lock...");

    // Read the bytes one at a time
    int c;      // buffer for read bytes
    while(read(fd, &c, 1) > 0) write(STDOUT_FILENO, &c, 1);     // write one byte to the standard output

    // Release the lock explicitly
    lock.l_type = F_UNLCK;
    if(fcntl(fd, F_SETLK, &lock) < 0) report_and_exit("explicit unlocking failed...");

    close(fd);
    return 0;
}