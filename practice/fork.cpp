#include <iostream>
#include <unistd.h>

int main()
{
    __pid_t pid;    // pid var
    pid = fork();   // call fork function

    if(pid > 0) std::cout << "parent process: " << getpid() << std::endl; // parent process
    else if(pid == 0)   // child process
    {
        std::cout << "child process: " << getpid() << std::endl;
        pid = fork();   // another process
        if(pid > 0) std::cout << "child process: " << getpid() << std::endl;    // child process itself
        else std::cout << "grand child: " << getpid() << std::endl;             // grand child process
    }

    else std::cout << "error!" << std::endl;
    return 0;
}