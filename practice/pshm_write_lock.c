/*************************************************************************\
*                  Copyright (C) Michael Kerrisk, 2019.                   *
*                                                                         *
* This program is free software. You may use, modify, and redistribute it *
* under the terms of the GNU General Public License as published by the   *
* Free Software Foundation, either version 3 or (at your option) any      *
* later version. This program is distributed without any warranty.  See   *
* the file COPYING.gpl-v3 for details.                                    *
\*************************************************************************/

/* Listing 54-2 */

/* pshm_write.c

   Usage: pshm_write shm-name string

   Copy 'string' into the POSIX shared memory object named in 'shm-name'.

   See also pshm_read.c.
*/
#include <fcntl.h>
#include <sys/mman.h>
#include "tlpi_hdr.h"
#include <time.h>
#include <semaphore.h>
#include <sys/file.h>
#include "curr_time.h"

int
main(int argc, char *argv[])
{
    int sem_fd = 0;
    size_t len;                 /* Size of shared memory object */
    char *addr;
    int sz = 100;
    int arr[100] = {0};
    int it = 0;
    len = sizeof(int);

    for(int i = 0; i < sz; i++) arr[i] = i;

    /* semaphore related variables */
	sem_t *sem;
	int sem_val=0;
	sem = sem_open("/demo",0);


    sem_fd = shm_open("/demo_shm", O_RDWR, 0);      /* Open existing object */
    if (sem_fd == -1)
        errExit("shm_open");
    if (ftruncate(sem_fd, len) == -1)           /* Resize object to hold string */
        errExit("ftruncate");

    addr = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, sem_fd, 0);
    if (addr == MAP_FAILED)
        errExit("mmap");

    /* file locking needs "touch tfile" beforehands */
    int lock_fd, lock;
    const char *lname;

    lock = LOCK_EX;
    // lock |= LOCK_NB;

    lock_fd = open("tfile", O_RDONLY);               /* Open file to be locked */
    if (lock_fd == -1) errExit("open");

    lname = (lock & LOCK_SH) ? "LOCK_SH" : "LOCK_EX";

    int tmp = 0 ;
    /* initialize shared variable */
    memcpy(addr, &tmp, len);             /* Copy string to shared memory */
    while(1){
  
		if(sem_getvalue(sem, &sem_val)==-1) errExit("sem_getvalue");
		while(sem_val>0) if(sem_getvalue(sem, &sem_val)==-1) errExit("sem_getvalue");
        
        /* flock part */
        if (flock(lock_fd, lock) == -1) {
            if (errno == EWOULDBLOCK)   fatal("PID %ld: already locked - bye!", (long) getpid());
            else                        errExit("flock (PID=%ld)", (long) getpid());
        }

        tmp = *addr;
        tmp++;
        printf("shared memory write data: %d\n", tmp);
        memcpy(addr, &tmp, len);             /* Copy string to shared memory */
        sleep(1);
        if (flock(lock_fd, LOCK_UN) == -1) errExit("flock");
        it++;
    }

    if (close(sem_fd) == -1)                    /* 'sem_fd' is no longer needed */
        errExit("close");
}
