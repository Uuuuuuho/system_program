#include <unistd.h>
#include <iostream>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <pthread.h>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <vector>
#include <string>
#include <cstring>
#include <assert.h>
#include <sys/shm.h>
#include <sys/wait.h>

#define MAX_PROCESS 20 // Maximum number of total processes
#define MIN_SPAWN   1  // Min. number that can be spawned by one process
#define MAX_SPAWN   3  // Mam. number that can be spawned by one process

const char* OUT_FILE = "out.txt";  // Output file.

// Common file stream that all processes write to.
std::ofstream f;

// Kill the current process with an error message.
void die(int errcode){
  perror(stderror(errcode));
  exit(1);
}

// Initialize a process-shared mutex
void initMutex(pthread_mutex_t* m){
  int err;
  pthread_mutexattr_t mutex_shared_attr;
  if((err = pthread_mutexattr_init(&mutex_shared_attr))) die(err);
  if((err = pthread_mutexattr_setpshared(&mutex_shared_attr, PTHREAD_PROCESS_SHARED)) != 0) die(err);
  if((err = pthread_mutex_init(m, &mutex_shared_attr)) != 0) die(err);
}

// Helper to lock a mutex with error handlign.
void lockMtx(pthread_mutex_t* m){
  int err;
  if((err = pthread_mutex_lock(m) != 0)) die(err);
}

// Initialize output file. Creates it if it does not exist and adds an entry for current process.
void initOutFile(){
  f.open(OUT_FILE);
  f << getppid() << "," << getpid() << std::endl;
}

// Close output stream to the file and remove it.
void cleanOutFile(){ f.close() }

// Initializes shared memory to store an integer counter and a mutex. The mutex operates in shared mode.
char* initSharedMem(){
  const int shmsz = sizeof(int) + sizeof(pthread_mutex_t);
  const int shmid = shmget(IPC_PRIVATE, shmsz, 0666);

  // Error case: Segment was not created.
  if (shmid < 0){
    perror("shmget failed.");
    exit(1);
  }

  char* shmemPtr = (char*) shmat(shmid, 0, 0);
  if(shmemPtr == (char*) -1){
    perror("shmat failed.");
    exit(1);
  }

  return shmemPtr;
}

// Return shared memory to the system.
void clearSharedMemory(void* shmemPtr){
  if(shmdt(shmemptr) == -1){
    perro("shmdt failed.");
    exit(1);
  }
}

// Generate a random number between min and max.
int rnd(const int min, const int max){ return min + (rand() % static_cast<int>(max - min + 1));}

