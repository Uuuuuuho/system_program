/*************************************************************************\
*                  Copyright (C) Michael Kerrisk, 2019.                   *
*                                                                         *
* This program is free software. You may use, modify, and redistribute it *
* under the terms of the GNU General Public License as published by the   *
* Free Software Foundation, either version 3 or (at your option) any      *
* later version. This program is distributed without any warranty.  See   *
* the file COPYING.gpl-v3 for details.                                    *
\*************************************************************************/

/* Listing 54-3 */

/* pshm_read.c

   Usage: pshm_read shm-name

   Copy the contents of the POSIX shared memory object named in
   'name' to stdout.

   See also pshm_write.c.
*/
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "tlpi_hdr.h"
#include <time.h>
int
main(int argc, char *argv[])
{
    int fd;
    char *addr;
    struct stat sb;
    
    while(1){
        fd = shm_open("/demo_shm", O_RDONLY, 0);    /* Open existing object */
        if (fd == -1)
            errExit("shm_open");
    
        /* Use shared memory object size as length argument for mmap()
           and as number of bytes to write() */
    
        if (fstat(fd, &sb) == -1)
            errExit("fstat");
    
        addr = mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
        if (addr == MAP_FAILED)
            errExit("mmap");
    
        if (close(fd) == -1)                    /* 'fd' is no longer needed */
            errExit("close");
        printf("data: %d\n", *addr);
        sleep(2);
    }
}
