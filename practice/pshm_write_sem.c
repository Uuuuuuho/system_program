/*************************************************************************\
*                  Copyright (C) Michael Kerrisk, 2019.                   *
*                                                                         *
* This program is free software. You may use, modify, and redistribute it *
* under the terms of the GNU General Public License as published by the   *
* Free Software Foundation, either version 3 or (at your option) any      *
* later version. This program is distributed without any warranty.  See   *
* the file COPYING.gpl-v3 for details.                                    *
\*************************************************************************/

/* Listing 54-2 */

/* pshm_write.c

   Usage: pshm_write shm-name string

   Copy 'string' into the POSIX shared memory object named in 'shm-name'.

   See also pshm_read.c.
*/
#include <fcntl.h>
#include <sys/mman.h>
#include "tlpi_hdr.h"
#include <time.h>
#include <semaphore.h>

int
main(int argc, char *argv[])
{
    int fd;
    size_t len;                 /* Size of shared memory object */
    char *addr;
    int sz = 100;
    int arr[100] = {0};
    int it = 0;
    for(int i = 0; i < sz; i++){
	arr[i] = i;
    }
    len = sizeof(int);

	sem_t *sem;
	sem = sem_open("/demo",0);


    fd = shm_open("/demo_shm", O_RDWR, 0);      /* Open existing object */
    if (fd == -1)
        errExit("shm_open");
    if (ftruncate(fd, len) == -1)           /* Resize object to hold string */
        errExit("ftruncate");

    addr = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (addr == MAP_FAILED)
        errExit("mmap");


	int sem_val=0;

    while(1){
  
		if(sem_getvalue(sem, &sem_val)==-1) errExit("sem_getvalue");
		while(sem_val>0) if(sem_getvalue(sem, &sem_val)==-1) errExit("sem_getvalue");
        printf("shared memory write data: %d\n", arr[it]);
        memcpy(addr, arr+it, len);             /* Copy string to shared memory */
        sleep(1);
        it++;
    }

    if (close(fd) == -1)                    /* 'fd' is no longer needed */
        errExit("close");
}
