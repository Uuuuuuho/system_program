#include <iostream>
#include <string.h>
// #include <stdio.h>
#include <chrono>
#include <unistd.h>
#include <stdlib.h>
#define MAX 200000000
using namespace std;

int main()
{
  int *src;
  int *dst;     // 메모리 복사  
  src = (int *)malloc(sizeof(int)*MAX);
  dst = (int *)malloc(sizeof(int)*MAX);

  auto start = chrono::steady_clock::now();

  // 여기에서 몇 가지 작업을 수행합니다.
  for(int i = 0; i < MAX; i++) src[i] = i;
  for(int i = 0; i < MAX; i++) dst[i] = 0;
  // memset(src,1,sizeof(src));
  // memset(dst,0,sizeof(dst));
  memcpy(dst, src, sizeof(src));    
  // sleep(3);

  auto end = chrono::steady_clock::now();

  cout << "Elapsed time in microseconds: "
      << chrono::duration_cast<chrono::microseconds>(end - start).count()
      << " us" << endl;

  cout << "Elapsed time in milliseconds: "
      << chrono::duration_cast<chrono::milliseconds>(end - start).count()
      << " ms" << endl;

  free(src);
  free(dst);
  return 0;
}