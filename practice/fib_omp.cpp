#include <cstdint>
#include <iostream>
#include <stdlib.h>

int64_t fib(int64_t n)
{
    if (n < 2)
    {
        return n;
    }
    else
    {
        int64_t x = 0, y = 0;
#pragma omp task shared(x,n)        
        x = fib(n-1);
#pragma omp task shared(y,n)
        y = fib(n-2);
#pragma omp taskwait        
        return x + y;
    }
}

int main(int argc, char* argv[])
{
    int64_t n = 43;
    int64_t result = fib(n);
    std::cout << "Fibonacci of " << n << " is " << result << std::endl;    
    return 0;
}